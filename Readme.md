##Project description

This is a test task. The latest apk you can download from [here](https://www.dropbox.com/s/sn23z47cgu46wrv/atendee_list_viewer.apk?dl=0).
Assets for publishing to play store located [here](https://bitbucket.org/vmalikov/festattendeelist/src/master/publishing_assets/)

###How it works
The app asks for location, bluetooth permissions on start up. *(Permissions for notification are not in dangerous group for Android, so it's allowed by default)*

- [Location permission explanation](http://prntscr.com/jm37rx) 
- [Location permission](http://prntscr.com/jm38cp) 
- [Entry point to start](http://prntscr.com/jm38rx)

After got or skip permission the app checks if device has connection. If yes - loads ACK and deviceID and user list from BE.
Otherwise tries to fetch cached data from DB. And show conference attendies list for the user. [User list](http://prntscr.com/jm39bs)

User can filter attendies by tags ('type' in the data model) and can check attendee details. 

- [Empty filter result](http://prntscr.com/jm39w5)
- [Not empty filter result](http://prntscr.com/jm3afl) 

From details screen there could be available options to make a phone call or write an email to specific public email if required data exists. [Details](http://prntscr.com/jm3b6m)

#####Design Solution Explanation
List should loads with pagination both from API and DB. 
All results stores in database to provide an access for all features in airplane mode. 
List presented with cards because it's modern way to show similar data and card allows to show much data. As this is conference app - it's important for user to see name, postion, company and tags (which determines if user and attendee have common interests)
Also cards hold enough place and could be replaced with advertisemed benner items or something like that.

Most popular filter items located at the top to get faster feedback for user. Other filters could be located in dropdown.
Search could be accessed from action bar top right corner and displays on new screen.

By click on item user can see details and there could be all information regarding attendee, and actions like email, call, likes, report, etc

The project is written in [Kotlin lang](https://kotlinlang.org/) and contains 3 modules:

- app - main module with all logic and UI
- redux - helps applying Redux architecture for presentation layer
- poweradapter - helps to show easy different data in lists

You can clone whole repo by run this command via terminal ```git clone git@bitbucket.org:vmalikov/festattendeelist.git```

###Configuration

Most of configs located under: festattendeelist/src/master/app/build.gradle. 

For applyng to play store each time you have to increment:
    - versionName
    - versionCode

There are 2 flavors for dev and prod.
Each of them contains:

- BASE_URL
- USER_LIST_ID
- apiClientId
- apiToken
  
###Basic tools

- Network communications: [retrofit](http://square.github.io/retrofit/)
- Json parsing: [gson](https://github.com/google/gson)
- Data storage: [requery](https://github.com/requery/requery)




