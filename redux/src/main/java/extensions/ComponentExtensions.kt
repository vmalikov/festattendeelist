package extensions

import android.arch.lifecycle.ViewModelProviders
import android.support.v4.app.Fragment
import com.justfordun.redux.*

fun <T : ReduxDipatcher<out State, ViewState>> Fragment.getDispatcher(clazz: Class<out DispatcherHolder<out State, ViewState, T>>): T {
    @Suppress("UNCHECKED_CAST")
    return (ViewModelProviders.of(this).get(clazz) as DispatcherHolder<out State, ViewState, ReduxDipatcher<out State, ViewState>>).dispatcher as T
}