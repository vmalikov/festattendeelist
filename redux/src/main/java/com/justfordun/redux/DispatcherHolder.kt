package com.justfordun.redux

import android.arch.lifecycle.ViewModel

abstract class DispatcherHolder<S : State, out VS : ViewState, out D : ReduxDipatcher<S, VS>>(val dispatcher: D) : ViewModel() {

    override fun onCleared() {
        dispatcher.onCleared()
        super.onCleared()
    }
}