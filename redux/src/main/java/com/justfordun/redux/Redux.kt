package com.justfordun.redux

import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject

interface Action

interface State

interface ViewState

interface Reducer<S : State> {
    fun reduce(oldState: S, action: Action): S
}

interface Store<S : State> {
    fun dispatch(action: Action)
    fun dispatch(actions: Observable<out Action>)
    fun asObservable(): Observable<S>
    fun currentState(): S
    fun tearDown()
}

class SimpleStore<S : State>(private val initialValue: S, reducer: Reducer<S>) : Store<S> {

    private val actionsSubject = PublishSubject.create<Action>()
    private val statesSubject = BehaviorSubject.create<S>()

    private val compositeSub = CompositeDisposable()

    init {
        compositeSub.add(actionsSubject
                .scan(initialValue, reducer::reduce)
                .distinctUntilChanged()
                .subscribe { statesSubject.onNext(it) }
        )

    }

    override fun dispatch(action: Action) {
        actionsSubject.onNext(action)
    }

    override fun dispatch(actions: Observable<out Action>) {
        compositeSub.add(actions.subscribe { dispatch(it) })
    }

    override fun asObservable(): Observable<S> = statesSubject

    override fun currentState(): S = if (statesSubject.hasValue()) statesSubject.value!! else initialValue

    override fun tearDown() {
        compositeSub.dispose()
    }
}