package com.justfordun.redux

import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class ReduxDipatcher<S : State, out VS : ViewState>(protected val store: Store<S>,
                                                             private val mainScheduler: Scheduler,
                                                             private val viewStateMapper: (S) -> VS) {
    /**
     * Override to transform the state
     */
    protected open val state: Observable<S> = store.asObservable()
    private val subscriptions = CompositeDisposable()

    protected fun addSubscription(subscription: Disposable) {
        subscriptions.add(subscription)
    }

    fun subscribe(onNext: (viewState: VS) -> Unit): Disposable = state
            .map(viewStateMapper)
            .observeOn(mainScheduler)
            .subscribe(onNext)

    fun onCleared() {
        store.tearDown()
        subscriptions.clear()
    }
}
