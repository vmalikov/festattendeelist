package co.festivality.attendeelist.data

import co.festivality.attendeelist.model.Model
import io.reactivex.Maybe

fun asMaybe(f: () -> List<Model.UserModelResponse>): Maybe<List<Model.UserModelResponse>> = Maybe.create { e -> e.onSuccess(f()) }