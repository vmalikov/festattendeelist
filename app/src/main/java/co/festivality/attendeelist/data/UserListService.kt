package co.festivality.attendeelist.data

import co.festivality.attendeelist.model.Model
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface UserListService {

    @GET("user-list/{id}")
    fun getUser(@Path("id") id: String): Single<Model.UserList>

    @GET("user-list")
    fun getUserList(): Single<Model.UserList>

    companion object {
        fun create(): UserListService {
            return Networking.retrofit.create(UserListService::class.java)
        }
    }
}