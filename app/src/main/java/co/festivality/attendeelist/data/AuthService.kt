package co.festivality.attendeelist.data

import co.festivality.attendeelist.model.Model
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.POST

interface AuthService {

    @POST("app/ack")
    fun getAck(): Observable<Model.ACK>

    @GET("app/deviceId")
    fun getDeviceId(): Observable<Model.DeviceId>

    companion object {
        fun create(): AuthService {
            return Networking.retrofit.create(AuthService::class.java)
        }
    }
}