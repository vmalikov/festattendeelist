package co.festivality.attendeelist.data

import co.festivality.attendeelist.model.Model
import io.reactivex.Maybe

object SearchHelper {

    private var memoryData = ArrayList<Model.UserModelResponse>()

    fun setData(data: List<Model.UserModelResponse>, clear: Boolean = false) {
        if (clear) memoryData.clear()

        if (!memoryData.containsAll(data)) memoryData.addAll(data)
    }

    fun getData(limit: Int, offset: Int): Maybe<List<Model.UserModelResponse>> = asMaybe {
        if (memoryData.size < offset + limit) return@asMaybe emptyList()
        memoryData.subList(offset, offset + limit)
    }

    fun filters(items: List<Model.UserModelType>, limit: Int, offset: Int): Maybe<List<Model.UserModelResponse>> = asMaybe {
        val result = memoryData.filter { items.intersect(it.type ?: emptyList()).isNotEmpty() }
        if (result.isNotEmpty()) {

            if(result.size > offset) {

                if(result.size > limit + offset) return@asMaybe result.subList(offset, limit)

                return@asMaybe result.subList(offset, result.size)
            }
        }
        emptyList()
    }
}