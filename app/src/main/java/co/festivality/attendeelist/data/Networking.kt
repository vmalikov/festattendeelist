package co.festivality.attendeelist.data

import android.text.TextUtils
import co.festivality.attendeelist.BuildConfig
import co.festivality.attendeelist.model.Model
import co.festivality.attendeelist.model.UserModelResponseDeserializer
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object Networking {
    var retrofit: Retrofit

    var deviceId: String = ""

    private val COOKIES_REQUEST_INTERCEPTOR = Interceptor { chain ->
        val request = chain.request()
        val builder = request.newBuilder()

        if(!TextUtils.isEmpty(deviceId)) {
            val jsonObj = JsonObject()
            jsonObj.addProperty("deviceId", deviceId)
            builder?.addHeader("x-header-request", jsonObj.toString())
        }

        chain.proceed(builder.build())
    }

    private val X_API_CLIENT_INTERCEPTOR = Interceptor { chain ->

        val jsonObj = JsonObject()
        jsonObj.addProperty("apiClientId", BuildConfig.apiClientId)
        jsonObj.addProperty("apiToken", BuildConfig.apiToken)

        val builder = chain.request().newBuilder()
                .addHeader("X-APIClient", jsonObj.toString())

        chain.proceed(builder.build())
    }

    init {
        val httpClient = OkHttpClient().newBuilder()
                .addInterceptor(COOKIES_REQUEST_INTERCEPTOR)
                .addInterceptor(X_API_CLIENT_INTERCEPTOR)
                .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))

        val gson = GsonBuilder()
                .registerTypeAdapter(Model.UserModelResponse::class.java, UserModelResponseDeserializer())
                .create()

        retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(BuildConfig.BASE_URL)
                .client(httpClient.build())
                .build()
    }
}