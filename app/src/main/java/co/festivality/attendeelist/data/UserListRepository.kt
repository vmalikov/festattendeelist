package co.festivality.attendeelist.data

import co.festivality.attendeelist.database.AppDatabase
import co.festivality.attendeelist.database.models.UserModel
import co.festivality.attendeelist.database.models.UserType
import co.festivality.attendeelist.database.models.fromDbUserModelMapper
import co.festivality.attendeelist.database.models.toDbUserModelMapperToDb
import co.festivality.attendeelist.model.Model
import com.justfordun.redux.Action
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.requery.kotlin.`in`
import io.requery.kotlin.eq
import timber.log.Timber

class UserListRepository(private val userListService: UserListService, private val scheduler: Scheduler) {

    private var copyingToDbInProgress: Boolean = false

    fun getUserList(limit: Int, offset: Int): Observable<AttendeeListActions> = mapDataToAction(
            SearchHelper.getData(limit, offset)
                    .filter { it.isNotEmpty() }
                    .switchIfEmpty(cachedUsersInDb(limit, offset))
                    .filter { it.isNotEmpty() }
                    .switchIfEmpty(apiUsers(limit, offset)), isLoadedMore(offset))

    fun filters(items: List<Model.UserModelType>, limit: Int, offset: Int): Observable<AttendeeListActions> = mapDataToAction(
            SearchHelper.filters(items, limit, offset)
                    .filter { it.isNotEmpty() }
                    .switchIfEmpty(filterUsersInDb(items, limit, offset)),
            isLoadedMore(offset))

    private fun apiUsers(limit: Int, offset: Int): Maybe<List<Model.UserModelResponse>> {
        return userListService.getUserList()
                .doOnSuccess { SearchHelper.setData(it.response) }
                .doOnSuccess { storeToDB(it.response) }
                .map { it.response.subList(offset, limit) }
                .toMaybe()
    }

    private fun cachedUsersInDb(limit: Int, offset: Int): Maybe<List<Model.UserModelResponse>> = asMaybe {
        val list = (AppDatabase.data select UserModel::class).limit(limit).offset(offset).get().toList()
        fromDbUserModelMapper(list)
    }

    private fun filterUsersInDb(items: List<Model.UserModelType>, limitValue: Int, offsetValue: Int): Maybe<List<Model.UserModelResponse>> = asMaybe {
        val typesTexts = items.map { it.text }
        val list = (
                (AppDatabase.data select UserModel::class join
                        UserType::class on UserType::userModelId.eq(UserModel::id))
                        where (UserType::text.`in`(typesTexts))
                        limit limitValue offset offsetValue)
                .get().toList()

        fromDbUserModelMapper(list)
    }

    private fun storeToDB(data: List<Model.UserModelResponse>) {
        copyingToDbInProgress = true
        val dataToStore = toDbUserModelMapperToDb(data)
        AppDatabase.data.upsert(dataToStore)
                .subscribeOn(scheduler)
                .observeOn(AndroidSchedulers.mainThread())
                .toObservable()
                .subscribe {
                    Timber.d("Stored into db!")
                    AppDatabase.data.count(UserModel::class).get().single().subscribe { i -> Timber.d("Now in db stored $i users") }
                    copyingToDbInProgress = false
                    SearchHelper.setData(emptyList(), true)
                }
    }

    private fun isLoadedMore(offset: Int) = offset != DEFAULT_OFFSET

    private fun mapDataToAction(maybe: Maybe<List<Model.UserModelResponse>>, isLoadedMore: Boolean = false): Observable<AttendeeListActions> = maybe
            .subscribeOn(scheduler)
            .map { AttendeeListActions.Success(it, isLoadedMore) as AttendeeListActions }
            .onErrorReturn { AttendeeListActions.Error(it) }
            .toObservable()
            .startWith(AttendeeListActions.Loading)

    sealed class AttendeeListActions : Action {
        data class Success(val userList: List<Model.UserModelResponse>?, val isLoadedMore: Boolean = false) : AttendeeListActions()
        data class Error(val error: Throwable) : AttendeeListActions()
        object Loading : AttendeeListActions()
    }

    companion object {
        const val PAGE_SIZE = 20
        const val DEFAULT_OFFSET = 0

        fun create(userListService: UserListService, scheduler: Scheduler): UserListRepository {
            return UserListRepository(userListService, scheduler)
        }
    }
}