package co.festivality.attendeelist.attendeedetails

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import co.festivality.attendeelist.MainActivity
import co.festivality.attendeelist.R
import co.festivality.attendeelist.extension.addScreen
import co.festivality.attendeelist.model.Model
import kotlinx.android.synthetic.main.screen_attendee_details_info_layout.*


open class DetailsInfoFragment : Fragment() {

    private lateinit var userModelCustomFields: Model.UserModelCustomFields

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.screen_attendee_details_info_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        userModelCustomFields.apply {

//            country_text.text = String.format("%s %s", getString(R.string.details_country), countryCode)
//            city_text.text = String.format("%s %s", getString(R.string.details_city), city)
//            age_text.text = String.format("%s %s", getString(R.string.details_age), age)
//            gender_text.text = String.format("%s %s", getString(R.string.details_gender), gender)

            send.setOnClickListener({ (activity as MainActivity).addScreen(MessageInfoFragment.newInstance(userModelCustomFields)) })
            call.setOnClickListener({ makeCall(phone) })
        }
    }

    private fun makeCall(phone: String?) {
        phone?.let {
            startActivity(Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null)))
        }
    }

    companion object {
        fun newInstance(userModelCustomFields: Model.UserModelCustomFields): DetailsInfoFragment {
            val f = DetailsInfoFragment()
            f.userModelCustomFields = userModelCustomFields
            return f
        }
    }
}
