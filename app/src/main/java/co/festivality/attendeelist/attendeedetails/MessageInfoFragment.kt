package co.festivality.attendeelist.attendeedetails

import android.content.ActivityNotFoundException
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import co.festivality.attendeelist.R
import co.festivality.attendeelist.model.Model
import kotlinx.android.synthetic.main.screen_attendee_details_message_layout.*
import timber.log.Timber


open class MessageInfoFragment : Fragment() {

    private lateinit var userModelCustomFields: Model.UserModelCustomFields

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.screen_attendee_details_message_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        send.setOnClickListener({
            if(!TextUtils.isEmpty(message_edit_text.text.toString())) {
                val emailIntent = Intent(Intent.ACTION_SENDTO)
                emailIntent.type = "text/html"
                emailIntent.putExtra(Intent.EXTRA_EMAIL, userModelCustomFields.publicEmail)
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.email_subject))
                emailIntent.putExtra(Intent.EXTRA_TEXT, message_edit_text.text.toString())

                try {
                    startActivity(Intent.createChooser(emailIntent, "Send Email"))
                } catch (e: ActivityNotFoundException) {
                    //TODO: Handle case where no email app is available
                    Timber.i(MessageInfoFragment::javaClass.name, e.message)
                }
            } else {
                Toast.makeText(context, "Can not send an email. Please write the message text in the input.", Toast.LENGTH_SHORT).show()
            }
        })
    }

    companion object {
        fun newInstance(userModelCustomFields: Model.UserModelCustomFields): MessageInfoFragment {
            val f = MessageInfoFragment()
            f.userModelCustomFields = userModelCustomFields
            return f
        }
    }
}
