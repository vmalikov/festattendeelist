package co.festivality.attendeelist.attendeedetails

import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import co.festivality.attendeelist.R
import co.festivality.attendeelist.model.Model
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.drawee.backends.pipeline.PipelineDraweeController
import com.facebook.imagepipeline.request.ImageRequestBuilder
import io.reactivex.disposables.CompositeDisposable
import jp.wasabeef.fresco.processors.BlurPostprocessor
import kotlinx.android.synthetic.main.screen_attendee_details.*


class AttendeeDetails : Fragment() {

    private val subscriptions = CompositeDisposable()
    private lateinit var user: Model.UserModelResponse

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.screen_attendee_details, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        user.customFields?.apply {
            title.text = fullName
            position_text.text = position
            company_text.text = company

            val uri = Uri.parse(user.media?.get(0)?.files?.variations?.original)

            val imageRequest = ImageRequestBuilder.newBuilderWithSource(uri)
                    .setPostprocessor(BlurPostprocessor(context, 1))
                    .build()

            user_image.controller = Fresco.newDraweeControllerBuilder()
                    .setImageRequest(imageRequest)
                    .setOldController(user_image.controller)
                    .build() as PipelineDraweeController

            val f = DetailsInfoFragment.newInstance(this)
            childFragmentManager
                    .beginTransaction()
                    .add(R.id.container, f, f.javaClass.simpleName)
                    .commit()
        }
    }

    override fun onStop() {
        subscriptions.clear()
        super.onStop()
    }

    companion object {
        fun newInstance(user: Model.UserModelResponse): AttendeeDetails {
            val f = AttendeeDetails()
            f.user = user
            return f
        }
    }
}