package co.festivality.attendeelist.model

import com.google.gson.*
import java.lang.reflect.Type

class UserModelResponseDeserializer : JsonDeserializer<Model.UserModelResponse> {

    private val customFieldsTagName = "customFields"

    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): Model.UserModelResponse {
        json?.let {
            it.asJsonObject?.get(customFieldsTagName)?.let {
                return when {
                    it.isJsonObject -> defaultParseUserModelResponse(json)
                    (it as JsonPrimitive).isNumber -> {
                        val userModel = defaultParseUserModelResponseWithoutCustomFields(json)
                        userModel.customFields = Model.UserModelCustomFields(isEmpty = true)
                        userModel
                    }
                    else -> defaultParseUserModelResponse(json)
                }
            }
        }

        return defaultParseUserModelResponse(json)
    }

    private fun defaultParseUserModelResponseWithoutCustomFields(json: JsonElement?): Model.UserModelResponse {
        return GsonBuilder()
                .addDeserializationExclusionStrategy(object : ExclusionStrategy {
                    override fun shouldSkipClass(clazz: Class<*>?) = false
                    override fun shouldSkipField(f: FieldAttributes?) = customFieldsTagName == f?.name
                })
                .create()
                .fromJson(json, Model.UserModelResponse::class.java)
    }

    private fun defaultParseUserModelResponse(json: JsonElement?): Model.UserModelResponse {
        return GsonBuilder()
                .create()
                .fromJson(json, Model.UserModelResponse::class.java)
    }
}