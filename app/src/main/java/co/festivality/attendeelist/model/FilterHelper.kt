package co.festivality.attendeelist.model

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import co.festivality.attendeelist.R
import co.festivality.attendeelist.ui.ColorHelper

object FilterHelper {
    private val appliedFilters = ArrayList<Model.UserModelType>()


    fun getFilterItems() = listOf(
            Model.UserModelType("#software"),
            Model.UserModelType("#design"),
            Model.UserModelType("#iOS dev"),
            Model.UserModelType("#Android dev")
    )

    fun getFilterView(context: Context, it: Model.UserModelType, callback: (ArrayList<Model.UserModelType>) -> Unit): TextView {
        val v = TextView(context)
        v.text = it.text
        v.tag = it
        v.setTextColor(Color.parseColor("#ffffff"))
        val padding = context.resources.getDimension(R.dimen.filter_panel_item_padding).toInt()
        v.setPadding(padding, padding, padding, padding)
        val lp = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        val margin = context.resources.getDimension(R.dimen.filter_panel_item_margin).toInt()
        lp.marginStart = margin
        lp.marginEnd = margin
        lp.topMargin = margin
        lp.bottomMargin = margin
        v.layoutParams = lp
        setBackgroundColorTo(v)
        v.setOnClickListener {
            val filter = v.tag as Model.UserModelType
            if (appliedFilters.contains(filter)) {
                appliedFilters.remove(filter)
                setBackgroundColorTo(v)
            } else {
                appliedFilters.add(filter)
                setBackgroundColorTo(v, ColorHelper.getRandomColor())
            }
            callback(appliedFilters)
        }
        return v
    }

    private fun setBackgroundColorTo(view: View, color: Int = Color.GRAY) {
        val drawable = GradientDrawable()
        drawable.cornerRadius = 1000f
        drawable.setColor(color)
        view.background = drawable
    }

}