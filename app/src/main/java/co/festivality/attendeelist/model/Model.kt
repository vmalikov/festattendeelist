package co.festivality.attendeelist.model

import io.neverstoplearning.poweradapter.item.RecyclerItem
import io.reactivex.internal.operators.maybe.MaybeIsEmpty

object Model {
    /**
     * ACK (auth stuff)
     */
    data class ACK(val status: String, val message: String,
                   val method: String, val userAuthenticated: Boolean,
                   val apiAuthenticated: Boolean, val host: String,
                   val path: String, val responseSize: Int, val response: List<ACKResponse>)

    data class ACKResponse(val message: String, val identifier: String)

    /**
     * Device id
     *
     */
    data class DeviceId(val status: String, val message: String,
                        val method: String, val userAuthenticated: Boolean,
                        val apiAuthenticated: Boolean, val host: String,
                        val path: String, val responseSize: Int, val response: DeviceIdResponse)

    data class DeviceIdResponse(val deviceId: String)


    /**
     * UserModel list
     */
    data class UserList(val status: String, val message: String,
                        val method: String, val userAuthenticated: Boolean,
                        val apiAuthenticated: Boolean, val host: String,
                        val path: String, val responseSize: Int, val response: List<UserModelResponse>)

    data class UserModelResponse(val id: String, val isFeatured: Int?,
                                 val type: List<UserModelType>?,
                                 var customFields: UserModelCustomFields?,
                                 val date: UserModelDate?,
                                 val likes: Int?,
                                 val media: List<UserModelMedia>?,
                                 val ilike: String?,
                                 val irate: String?) : RecyclerItem {

        override fun getId(): Long = id.toLong()
        override fun renderKey(): String = UserModelResponse.renderKey()

        companion object {
            fun renderKey(): String = "UserModelResponse"
        }
    }

    data class UserModelType(val text: String?)

    data class UserModelCustomFields(val fullName: String = "", val firstName: String = "", val lastName: String = "",
                                     val email: String = "", val publicEmail: String = "", val company: String = "",
                                     val position: String = "", val gender: String = "", val countryCode: String = "",
                                     val phone: String = "", val city: String = "", val age: Int = 0, val isEmpty: Boolean = false)

    data class UserModelDate(val startDate: String?, val endDate: String?)

    data class UserModelMedia(val type: String?, val label: String?, val files: UserModelMediaFiles?)

    data class UserModelMediaFiles(val default: String?, val variations: UserModelMediaFilesVariations?)

    data class UserModelMediaFilesVariations(val small: String?, val original: String?, val tiny: String?)
}