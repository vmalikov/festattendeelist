package co.festivality.attendeelist.ui

import android.graphics.Color
import java.util.*

object ColorHelper {
    private val allColors = listOf<String>("#D32F2F", "#E91E63",
            "#880E4F", "#9C27B0", "#4A148C", "#673AB7", "#3F51B5", "#2196F3", "#03A9F4", "#00BCD4",
            "#009688", "#4CAF50", "#8BC34A", "#CDDC39", "#FFEB3B", "#FFC107", "#FF9800", "#FF5722", "#795548")

    fun getRandomColor(): Int {
        return Color.parseColor(allColors[Random().nextInt(allColors.size)])
    }
}