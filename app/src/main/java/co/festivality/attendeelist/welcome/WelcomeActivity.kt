package co.festivality.attendeelist.welcome

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import co.festivality.attendeelist.MainActivity
import co.festivality.attendeelist.R
import co.festivality.attendeelist.data.AuthService
import co.festivality.attendeelist.data.Networking
import co.festivality.attendeelist.extension.isConnected
import co.festivality.attendeelist.extension.removeScreen
import co.festivality.attendeelist.extension.showScreen
import co.festivality.attendeelist.permissions.PermissionManager
import com.jackandphantom.blurimage.BlurImage
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.screen_welcome_root.*

class WelcomeActivity : AppCompatActivity(), WelcomeButtonRouter {

    val permissionsRequestCode: Int = 1

    var permissionListener: RequestPermissionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.screen_welcome_root)

        actionBar?.title = ""
        supportActionBar?.title = ""

        prepareBeforeSplash()
        showScreen(SimpleSplashFragment.newInstance(R.layout.screen_splash, R.id.splash_image, object : OnSplashComplete {
            override fun onComplete(f: Fragment) {
                doAfterSplash(f)
            }
        }))
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {
        permissionListener?.let {
            when (requestCode) {
                permissionsRequestCode -> {

                    if (permissions.isNotEmpty()) {
                        val deniedPermissions = PermissionManager.getDeniedPermissions(permissions, grantResults)

                        if (deniedPermissions.isEmpty()) {
                            it.onPermissionGranted()
                        } else {
                            Toast.makeText(applicationContext, "Didn't allow permissions", Toast.LENGTH_SHORT).show()
                        }
                    }
                }
            }
        }
    }

    override fun onButtonClick(id: Int) {
        when (id) {
            R.id.enable_location -> askForPermissions(provideLocationPermissionListener(), listOf(PermissionManager.FLOW_PERMISSIONS[0], PermissionManager.FLOW_PERMISSIONS[1]))
            R.id.enable_bluetooth -> askForPermissions(provideLocationPermissionListener(), listOf(PermissionManager.FLOW_PERMISSIONS[2]))
            R.id.skip -> goToNextSlideIfCan()
            R.id.start_using_app -> loadAndEnterToTheApp()
        }
    }

    private fun loadAndEnterToTheApp() {
        if (!isConnected()) {
            startActivity(MainActivity.newIntent(this))
            finish()
            return
        }

        progressBar.visibility = View.VISIBLE
        AuthService.create().getAck()
                .subscribeOn(Schedulers.io())
                .flatMap { AuthService.create().getDeviceId() }
                .map { Networking.deviceId = it.response.deviceId }
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError {
                    progressBar.visibility = View.GONE
                    it.printStackTrace()
                    error_text.visibility = View.VISIBLE
                    error_text.text = getString(R.string.general_error)
                }
                .subscribe {
                    progressBar.visibility = View.GONE
                    error_text.visibility = View.GONE
                    startActivity(MainActivity.newIntent(this))
                    finish()
                }
    }

    private fun provideLocationPermissionListener(): RequestPermissionListener {
        return object : RequestPermissionListener {
            override fun onPermissionGranted() {
                goToNextSlideIfCan()
            }
        }
    }

    private fun goToNextSlideIfCan(): Boolean {
        val currentItem = view_pager.currentItem
        val totalAmount = view_pager.adapter?.count ?: 0
        if (currentItem < totalAmount - 1) {
            view_pager.currentItem = currentItem + 1
            return true
        }
        return false
    }

    private fun prepareBeforeSplash() {
        view_pager.visibility = View.GONE
        actionBar?.title
    }

    private fun doAfterSplash(f: Fragment) {
        BlurImage.with(applicationContext).load(R.drawable.welcome_background).intensity(8f).Async(false).into(image_back)
        removeScreen(f)
        view_pager.adapter = WelcomePagerAdapter(this@WelcomeActivity, supportFragmentManager)
        view_pager.visibility = View.VISIBLE
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 1) {
            supportFragmentManager.popBackStack()
        } else {
            finish()
        }
    }
}

interface OnSplashComplete {
    fun onComplete(f: Fragment)
}

interface WelcomeButtonRouter {
    fun onButtonClick(id: Int)
}

interface RequestPermissionListener {
    fun onPermissionGranted()
}