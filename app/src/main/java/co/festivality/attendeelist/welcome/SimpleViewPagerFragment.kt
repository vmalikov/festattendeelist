package co.festivality.attendeelist.welcome

import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.content.Context
import android.os.Bundle
import android.support.annotation.IdRes
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateInterpolator

open class SimpleViewPagerFragment : Fragment() {

    private var router: WelcomeButtonRouter? = null

    private var animatedView: View? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val args = arguments ?: throw IllegalArgumentException()

        val rootView = inflater.inflate(args.getInt(LAYOUT_KEY), container, false)

        val btnId = args.getInt(BUTTON_KEY)
        if (btnId != -1 && rootView.findViewById<View>(btnId) != null) {
            val view = rootView.findViewById<View>(btnId)
            view.setOnClickListener { view ->
                if (router != null) {
                    router!!.onButtonClick(view.id)
                }
            }

            if(args.getBoolean(ANIMATE_BUTTON_KEY)) {
                animateView(view)
            }
        }

        val secondBtnId = args.getInt(SECOND_BUTTON_KEY)
        if (secondBtnId != -1 && rootView.findViewById<View>(secondBtnId) != null) {
            rootView.findViewById<View>(secondBtnId).setOnClickListener { view ->
                if (router != null) {
                    router!!.onButtonClick(view.id)
                }
            }
        }

        return rootView
    }

    override fun onDetach() {
        super.onDetach()
        animatedView?.clearAnimation()
    }

    private fun animateView(view: View) {
        animatedView = view

        view.pivotX = 0.0f
        view.pivotY = view.height / 2.0f

        val rightValue = 2f
        val leftValue = -rightValue
        val duration = 1000L

        val animatorInit = ObjectAnimator.ofFloat(view, "rotationY", rightValue, leftValue)
        animatorInit.duration = duration
        animatorInit.interpolator = AccelerateInterpolator()
        animatorInit.repeatMode = ValueAnimator.REVERSE
        animatorInit.repeatCount = ValueAnimator.INFINITE
        animatorInit.start()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        if (context is WelcomeButtonRouter) {
            router = context
        }
    }

    companion object {
        const val LAYOUT_KEY = "LAYOUT_KEY"
        const val BUTTON_KEY = "BUTTON_KEY"
        const val SECOND_BUTTON_KEY = "SECOND_BUTTON_KEY"
        const val ANIMATE_BUTTON_KEY = "ANIMATE_BUTTON_KEY"

        fun newInstance(@LayoutRes layout: Int, @IdRes btnId: Int = -1, @IdRes secondBtnId: Int = -1, animateButton: Boolean = false): Fragment {
            val args = newBundle(layout, btnId, secondBtnId, animateButton)

            val fragment = SimpleViewPagerFragment()
            fragment.arguments = args
            return fragment
        }

        fun newBundle(@LayoutRes layout: Int, @IdRes btnId: Int, @IdRes secondBtnId: Int = -1, animateButton: Boolean ): Bundle {
            val args = Bundle()
            args.putInt(LAYOUT_KEY, layout)
            args.putInt(BUTTON_KEY, btnId)
            args.putInt(SECOND_BUTTON_KEY, secondBtnId)
            args.putBoolean(ANIMATE_BUTTON_KEY, animateButton)
            return args
        }
    }
}
