package co.festivality.attendeelist.welcome

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.os.Bundle
import android.support.annotation.IdRes
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView

class SimpleSplashFragment : SimpleViewPagerFragment() {

    private var onSplashComplete: OnSplashComplete? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val args = arguments ?: throw IllegalArgumentException()
        return inflater.inflate(args.getInt(LAYOUT_KEY), container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.let {
            val btnId = it.getInt(BUTTON_KEY)
            if (btnId != -1 && view.findViewById<ImageView>(btnId) != null) {
                val imageView = view.findViewById<ImageView>(btnId)

                val alphaAnimator = ObjectAnimator.ofFloat(imageView, "alpha", 0f, 1f)

                val scaleFactor = 2.5f
                val animationDuration = 1000L
                val scaleXAnimator = ObjectAnimator.ofFloat(imageView, "scaleX", 0f, scaleFactor)
                val scaleYAnimator = ObjectAnimator.ofFloat(imageView, "scaleY", 0f, scaleFactor)

                val animatorSet = AnimatorSet()
                animatorSet.play(alphaAnimator).with(scaleXAnimator).with(scaleYAnimator)
                animatorSet.duration = animationDuration
                animatorSet.start()

                animatorSet.addListener(object : Animator.AnimatorListener {
                    override fun onAnimationRepeat(animation: Animator?) {}

                    override fun onAnimationCancel(animation: Animator?) {}

                    override fun onAnimationStart(animation: Animator?) {}

                    override fun onAnimationEnd(animation: Animator?) {
                        onSplashComplete?.onComplete(this@SimpleSplashFragment)
                    }
                })
            }
        }
    }

    companion object {
        fun newInstance(@LayoutRes layout: Int, @IdRes imageId: Int = -1, onSplashComplete: OnSplashComplete? = null): Fragment {
            val args = SimpleViewPagerFragment.newBundle(layout, imageId, -1, false)

            val fragment = SimpleSplashFragment()
            fragment.onSplashComplete = onSplashComplete
            fragment.arguments = args
            return fragment
        }
    }
}
