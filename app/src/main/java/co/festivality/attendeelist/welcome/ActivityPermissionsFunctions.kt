package co.festivality.attendeelist.welcome

import android.support.v4.app.ActivityCompat
import co.festivality.attendeelist.permissions.PermissionManager

fun WelcomeActivity.askForPermissions(listener: RequestPermissionListener?, permissions: List<String>) {

    if (listener == null || permissions == null) {
        return
    }

    val filteredPermissions = PermissionManager.filterPermissions(applicationContext, permissions)

    if (filteredPermissions.isEmpty()) {
        listener.onPermissionGranted()
    } else {
        permissionListener = listener
        ActivityCompat.requestPermissions(this, filteredPermissions.toTypedArray(), permissionsRequestCode)
    }
}