package co.festivality.attendeelist.welcome

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import co.festivality.attendeelist.R
import co.festivality.attendeelist.permissions.PermissionManager
import java.util.ArrayList

class WelcomePagerAdapter internal constructor(context: Context, fm: FragmentManager) : FragmentPagerAdapter(fm) {

    private val fragments: ArrayList<Fragment> = ArrayList()

    init {
        if(!PermissionManager.isLocationPermissionGrant(context)) fragments.add(SimpleViewPagerFragment.newInstance(R.layout.screen_welcome_location_permission, R.id.enable_location, R.id.skip))
        if(!PermissionManager.isBluetoothPermissionGrant(context)) fragments.add(SimpleViewPagerFragment.newInstance(R.layout.screen_welcome_bleutooth_permission, R.id.enable_bluetooth, R.id.skip))
        fragments.add(SimpleViewPagerFragment.newInstance(R.layout.screen_welcome_layout, R.id.start_using_app, -1, true))
    }

    override fun getCount(): Int {
        return fragments.size
    }

    override fun getItem(position: Int): Fragment {
        return fragments[position]
    }
}