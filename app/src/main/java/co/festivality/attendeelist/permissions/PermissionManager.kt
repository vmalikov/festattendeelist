package co.festivality.attendeelist.permissions

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.support.v4.content.ContextCompat
import java.util.*

object PermissionManager {

    val FLOW_PERMISSIONS = arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.BLUETOOTH)

    fun isLocationPermissionGrant(context: Context): Boolean
            = isPermissionGrant(context, FLOW_PERMISSIONS[0]) && isPermissionGrant(context, FLOW_PERMISSIONS[1])

    fun isBluetoothPermissionGrant(context: Context): Boolean
            = isPermissionGrant(context, FLOW_PERMISSIONS[2])

    private fun isPermissionGrant(context: Context, permissionToCheck: String)
            = ContextCompat.checkSelfPermission(context, permissionToCheck) == PackageManager.PERMISSION_GRANTED


    fun filterPermissions(context: Context, permissions: List<String>): List<String> {
        val filteredList = ArrayList<String>()

        permissions
                .filter { ContextCompat.checkSelfPermission(context, it) != PackageManager.PERMISSION_GRANTED }
                .map { filteredList.add(it) }

        return filteredList
    }

    fun getDeniedPermissions(permissions: Array<String>, grantResults: IntArray, ignored: ArrayList<String> = ArrayList()): Array<String> {
        val deniedList = getDeniedPermissionsList(permissions, grantResults, ignored)
        return deniedList.toTypedArray()
    }

    private fun getDeniedPermissionsList(permissions: Array<String>, grantResults: IntArray, ignored: ArrayList<String>): List<String> {
        val deniedList = ArrayList<String>()

        for (i in permissions.indices) {
            if (grantResults[i] == PackageManager.PERMISSION_DENIED && !shouldBeIgnored(ignored, permissions[i])) {
                deniedList.add(permissions[i])
            }
        }

        return deniedList
    }

    private fun shouldBeIgnored(ignored: ArrayList<String>?, permission: String): Boolean {
        return ignored != null && ignored.contains(permission)
    }
}
