package co.festivality.attendeelist.extension

import android.content.Context
import android.net.ConnectivityManager
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import co.festivality.attendeelist.R


fun AppCompatActivity.showScreen(f: Fragment) {
    supportFragmentManager
            .beginTransaction()
            .replace(R.id.content_frame, f, f.javaClass.simpleName)
            .addToBackStack(f.javaClass.simpleName)
            .commit()
}

fun AppCompatActivity.addScreen(f: Fragment) {
    supportFragmentManager
            .beginTransaction()
            .add(R.id.content_frame, f, f.javaClass.simpleName)
            .addToBackStack(f.javaClass.simpleName)
            .commit()
}

fun AppCompatActivity.removeScreen(f: Fragment) {
    supportFragmentManager
            .beginTransaction()
            .remove(f)
            .commit()
}

fun AppCompatActivity.isConnected(): Boolean {
    val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val activeNetwork = cm.activeNetworkInfo

    return activeNetwork != null && activeNetwork.isConnectedOrConnecting
}