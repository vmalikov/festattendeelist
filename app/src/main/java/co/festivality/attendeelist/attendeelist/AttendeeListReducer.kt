package co.festivality.attendeelist.attendeelist

import co.festivality.attendeelist.data.UserListRepository
import co.festivality.attendeelist.data.UserListRepository.AttendeeListActions
import co.festivality.attendeelist.data.UserListService
import co.festivality.attendeelist.model.Model
import com.justfordun.redux.*
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Dispatcher holder
 */
class MainDispatcherHolder : DispatcherHolder<AttendeeListState, AttendeeListViewState, AttendeeListDispatcher>(
        AttendeeListDispatcher(UserListRepository.create(UserListService.create(), Schedulers.io()),
                SimpleStore(emptyState, MainReducer),
                AndroidSchedulers.mainThread()
        )
)

/**
 * Dispatcher class
 */
class AttendeeListDispatcher(private val repository: UserListRepository,
                             private val simpleStore: Store<AttendeeListState>,
                             scheduler: Scheduler)
    : ReduxDipatcher<AttendeeListState, AttendeeListViewState>(
        simpleStore,
        scheduler,
        ::mapStateToViewState) {

    fun loadUsers(filters: List<Model.UserModelType> = emptyList(), limit: Int = UserListRepository.PAGE_SIZE, offset: Int = UserListRepository.DEFAULT_OFFSET) {
        if(filters.isNotEmpty()) {
            simpleStore.dispatch(repository.filters(filters, limit, offset))
        } else {
            simpleStore.dispatch(repository.getUserList(limit, offset))
        }
    }
}

/**
 * Reducer
 */
object MainReducer : Reducer<AttendeeListState> {

    override fun reduce(oldState: AttendeeListState, action: Action): AttendeeListState {
        return when (action) {
            is AttendeeListActions.Loading -> emptyState.copy(isLoading = true)
            is AttendeeListActions.Error -> emptyState.copy(error = action.error)
            is AttendeeListActions.Success -> emptyState.copy(attendeeList = action.userList, isLoadedMore = action.isLoadedMore)
            else -> oldState
        }
    }
}

/**
 * State
 */
data class AttendeeListState(val attendeeList: List<Model.UserModelResponse>?, val error: Throwable?, val isLoading: Boolean, val isLoadedMore: Boolean) : State

val emptyState = AttendeeListState(attendeeList = ArrayList(), error = null, isLoading = false, isLoadedMore = false)

/**
 * View state
 */
data class AttendeeListViewState(val attendeeList: List<Model.UserModelResponse>?, val error: Throwable?, val isLoading: Boolean, val isLoadedMore: Boolean) : ViewState

/**
 * Mapper State to ViewState
 */
fun mapStateToViewState(state: AttendeeListState): AttendeeListViewState =
        AttendeeListViewState(state.attendeeList, state.error, state.isLoading, state.isLoadedMore)