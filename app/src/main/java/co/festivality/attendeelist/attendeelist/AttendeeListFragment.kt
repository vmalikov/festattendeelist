package co.festivality.attendeelist.attendeelist

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import co.festivality.attendeelist.MainActivity
import co.festivality.attendeelist.R
import co.festivality.attendeelist.attendeedetails.AttendeeDetails
import co.festivality.attendeelist.data.UserListRepository.Companion.PAGE_SIZE
import co.festivality.attendeelist.extension.addScreen
import co.festivality.attendeelist.model.FilterHelper
import co.festivality.attendeelist.model.Model
import com.justfordun.redux.DispatcherBinder
import extensions.getDispatcher
import io.neverstoplearning.poweradapter.adapter.RecyclerAdapter
import io.neverstoplearning.poweradapter.adapter.RecyclerDataSource
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.screen_attendee_list.*
import timber.log.Timber


interface ItemClickListener {
    fun onClick(userModel: Model.UserModelResponse)
}

/**
 * Screen which represents a list of attendee
 */
class AttendeeListFragment : Fragment() {

    private val mainDispatcher by lazy { getDispatcher(MainDispatcherHolder::class.java) }
    private val subscriptions = CompositeDisposable()

    private val attendeeListDataSource: RecyclerDataSource = RecyclerDataSource(mapOf(Model.UserModelResponse.renderKey() to AttendeeRenderer(itemClickListener())))

    private lateinit var layoutManager: LinearLayoutManager
    private var isLoading = false
    private var isLastPage = false

    private var appliedFilters: ArrayList<Model.UserModelType> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.screen_attendee_list, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initList()
        setupReaction()
        setupFilter()
        initLoading()
    }

    private fun setupFilter() {
        FilterHelper.getFilterItems().map {
            filter_panel.addView(FilterHelper.getFilterView(context!!, it, ::callback))
        }
    }

    private fun callback(filters: ArrayList<Model.UserModelType>) {
        appliedFilters.clear()
        appliedFilters.addAll(filters)
        isLastPage = false
        mainDispatcher.loadUsers(filters = appliedFilters)
    }

    private fun initLoading() {
        isLoading = true
        mainDispatcher.loadUsers()
    }

    private fun setupReaction() {
        DispatcherBinder(lifecycle, mainDispatcher) { mainViewState ->

            progressBar.visibility = if (mainViewState.isLoading) View.VISIBLE else View.GONE
            if (mainViewState.isLoading) return@DispatcherBinder

            if (mainViewState.error != null) {
                error_text.visibility = View.VISIBLE
                error_text.text = mainViewState.error.message
                Timber.e(mainViewState.error)
                return@DispatcherBinder
            } else {
                error_text.visibility = View.GONE
            }

            mainViewState.attendeeList?.let {

                showPlaceholder(it.isEmpty())

                if (mainViewState.isLoadedMore) {
                    attendeeListDataSource.appendData(it)
                    isLastPage = it.size < PAGE_SIZE
                } else {
                    attendeeListDataSource.setData(it)
                }

                isLoading = false
            }
        }
    }

    private fun showPlaceholder(show: Boolean) {
        no_data_placeholder.visibility = if (show) View.VISIBLE else View.GONE
    }

    override fun onStop() {
        subscriptions.clear()
        super.onStop()
    }

    private fun initList() {
        layoutManager = LinearLayoutManager(context)
        list.layoutManager = layoutManager
        list.adapter = RecyclerAdapter(attendeeListDataSource)
        list.addOnScrollListener(recyclerViewOnScrollListener)
    }

    private val recyclerViewOnScrollListener = object : RecyclerView.OnScrollListener() {

        override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            val visibleItemCount = layoutManager.childCount
            val totalItemCount = layoutManager.itemCount
            val firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition()

            val isTimeToLoadMore = (visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                    && firstVisibleItemPosition >= 0

            if (canLoadMore() && isTimeToLoadMore) {
                mainDispatcher.loadUsers(offset = totalItemCount, filters = appliedFilters)
                isLoading = true
            }
        }
    }

    private fun canLoadMore() = !isLoading && !isLastPage

    private fun itemClickListener(): ItemClickListener {
        return object : ItemClickListener {
            override fun onClick(userModel: Model.UserModelResponse) {
                (activity as MainActivity).addScreen(AttendeeDetails.newInstance(userModel))
            }
        }
    }
}