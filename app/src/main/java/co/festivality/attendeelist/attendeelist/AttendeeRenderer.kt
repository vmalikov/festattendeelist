package co.festivality.attendeelist.attendeelist

import android.graphics.drawable.GradientDrawable
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import co.festivality.attendeelist.R
import co.festivality.attendeelist.model.Model
import co.festivality.attendeelist.ui.ColorHelper
import com.facebook.drawee.view.SimpleDraweeView
import com.jakewharton.rxbinding.view.clicks
import io.neverstoplearning.poweradapter.item.ItemRenderer
import kotlinx.android.synthetic.main.attendee_item.view.*


class AttendeeRenderer(private val itemClickListener: ItemClickListener) : ItemRenderer<Model.UserModelResponse> {

    override fun layoutRes(): Int = R.layout.attendee_item

    override fun createView(parent: ViewGroup): View {
        val view = LayoutInflater.from(parent.context).inflate(layoutRes(), parent, false)
        view.tag = ViewBinder(view, itemClickListener)
        return view
    }

    override fun render(itemView: View, item: Model.UserModelResponse) {
        (itemView.tag as ViewBinder).bind(item)
    }

    internal class ViewBinder(private val itemView: View, private val itemClickListener: ItemClickListener) {
        private lateinit var user: Model.UserModelResponse

        fun bind(user: Model.UserModelResponse) {
            this.user = user

            user.customFields?.let {
                itemView.username.text = it.fullName
                itemView.position.text = String.format("%s @ %s", it.position, it.company)
            }

            val uri = Uri.parse(user.media?.get(0)?.files?.default)
            (itemView.avatar as SimpleDraweeView).setImageURI(uri)

            var date = "${user.date?.startDate} - ${user.date?.endDate}"
            if(date.contains("null")) {
                date = itemView.resources.getString(R.string.no_date)
            }
            itemView.text_date.text = date

            user.type?.let {

                val filteredList = it.toSet().toList()
                if (filteredList.size > 1) {
                    itemView.filter_first.text = filteredList[0].text
                    setBackgroundColorTo(itemView.filter_first)

                    itemView.filter_second.text = filteredList[1].text
                    setBackgroundColorTo(itemView.filter_second)

                } else if(filteredList.size == 1) {
                    itemView.filter_first.text = filteredList[0].text
                    setBackgroundColorTo(itemView.filter_first)

                    itemView.filter_second.text = ""
                    itemView.filter_second.background = null
                }
            }

            itemView.text_likes_count.text = "${user.likes ?: 0}"

            itemView.clicks()
                    .subscribe { itemClickListener.onClick(user) }
        }

        private fun setBackgroundColorTo(view: View) {
            val drawable = GradientDrawable()
            drawable.cornerRadius = 1000f
            drawable.setColor(ColorHelper.getRandomColor())
            view.background = drawable
        }
    }
}