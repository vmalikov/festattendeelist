package co.festivality.attendeelist.database.models

import android.os.Parcelable
import io.requery.*

@Entity
interface UserDate : Parcelable, Persistable {

    @get:Key
    @get:Generated
    var id: Int

    var startDate: String?
    var endDate: String?

    @get:OneToOne(mappedBy = "userDate")
    var owner: UserModel
}