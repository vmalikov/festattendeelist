package co.festivality.attendeelist.database.models

import co.festivality.attendeelist.model.Model


/**
 * Mappers from API models to DB models
 */
fun toDbUserModelMapperToDb(apiUsers: List<Model.UserModelResponse>): List<UserModel> {
    val users = ArrayList<UserModel>()

    apiUsers.map {
        users.add(UserModelEntity().apply {
            id = it.id
            isFeatured = it.isFeatured
            this.type = toDbUserType(it.type, this@apply)
            customFields = toDbUserCustomFieldsMapper(it.customFields)
            userDate = toDbUserDateMapper(it.date)
            likes = it.likes
            this.media = toDbUserMediaMapper(it.media, this@apply)
            ilike = it.ilike
            irate = it.irate
        })
    }
    return users
}

fun toDbUserType(type: List<Model.UserModelType>?, userModelEntity: UserModelEntity): List<UserType> {
    val typeList = ArrayList<UserType>()
    type?.map {
        typeList.add(UserTypeEntity().apply {
            text = it.text
            userModelId = userModelEntity.id
        })
    }

    return typeList
}

fun toDbUserCustomFieldsMapper(customFields: Model.UserModelCustomFields?): UserCustomFields {
    return UserCustomFieldsEntity().apply {
        customFields?.let {
            fullName = it.fullName
            firstName = it.firstName
            lastName = it.lastName
            email = it.email
            publicEmail = it.publicEmail
            company = it.company
            position = it.position
            gender = it.gender
            countryCode = it.countryCode
            phone = it.phone
            city = it.city
            age = it.age
        }
    }
}

fun toDbUserDateMapper(date: Model.UserModelDate?): UserDate {
    return UserDateEntity().apply {
        date?.let {
            startDate = it.startDate
            endDate = it.endDate
        }
    }
}

fun toDbUserMediaMapper(apiMedia: List<Model.UserModelMedia>?, owner: UserModel): List<UserMedia> {
    val media = ArrayList<UserMedia>()

    apiMedia?.map {
        media.add(UserMediaEntity().apply {
            type = it.type
            label = it.label

            this.files = toDbUserMediaFilesMapper(it.files, this@apply)
            this.owner = owner
            userModelId = owner.id
        })
    }
    return media
}

fun toDbUserMediaFilesMapper(files: Model.UserModelMediaFiles?, owner: UserMedia): UserMediaFiles? {
    return UserMediaFilesEntity().apply {
        files?.let {
            default = it.default
            variations = toDbUserMediaFilesVariantsMapper(it.variations, this@apply)
            userMedia = owner
        }
    }
}

fun toDbUserMediaFilesVariantsMapper(variations: Model.UserModelMediaFilesVariations?, owner: UserMediaFiles): UserMediaFilesVariations? {
    return UserMediaFilesVariationsEntity().apply {
        variations?.let {
            small = it.small
            original = it.original
            tiny = it.tiny
            userMediaFiles = owner
        }
    }
}

/**
 * Mappers from DB models to API models
 */
fun fromDbUserModelMapper(dbUsers: List<UserModel>): List<Model.UserModelResponse> {
    val userList = ArrayList<Model.UserModelResponse>()

    dbUsers.map {
        userList.add(Model.UserModelResponse(
                it.id,
                it.isFeatured,
                userTypesMapper(it.type),
                userCustomFieldsMapper(it.customFields),
                userDateMapper(it.userDate),
                it.likes,
                userMediaMapper(it.media),
                it.ilike,
                it.irate
        ))
    }
    return userList
}

private fun userTypesMapper(dbTypes: List<UserType>?): List<Model.UserModelType>? {
    val userTypes = ArrayList<Model.UserModelType>()

    dbTypes?.map {
        userTypes.add(Model.UserModelType(
                it.text))
    }

    return userTypes
}

private fun userCustomFieldsMapper(dbIt: UserCustomFields?): Model.UserModelCustomFields? {
    return Model.UserModelCustomFields(dbIt?.fullName ?: "", dbIt?.firstName ?: "",
            dbIt?.lastName ?: "", dbIt?.email ?: "", dbIt?.publicEmail ?: "",
            dbIt?.company ?: "", dbIt?.position ?: "", dbIt?.gender ?: "",
            dbIt?.countryCode ?: "", dbIt?.phone ?: "", dbIt?.city ?: "", dbIt?.age ?: 0)
}

private fun userDateMapper(dbIt: UserDate?): Model.UserModelDate? {
    return Model.UserModelDate(dbIt?.startDate, dbIt?.endDate)
}

private fun userMediaMapper(dbIt: List<UserMedia>?): List<Model.UserModelMedia>? {
    val mediaList = ArrayList<Model.UserModelMedia>()

    dbIt?.map {
        mediaList.add(Model.UserModelMedia(it.type, it.label, userMediaFilesMapper(it.files)))
    }
    return mediaList
}

private fun userMediaFilesMapper(dbIt: UserMediaFiles?): Model.UserModelMediaFiles? {
    return Model.UserModelMediaFiles(dbIt?.default, userMediaVariantsMapper(dbIt?.variations))
}

private fun userMediaVariantsMapper(dbIt: UserMediaFilesVariations?): Model.UserModelMediaFilesVariations? {
    return Model.UserModelMediaFilesVariations(dbIt?.small, dbIt?.original, dbIt?.tiny)
}