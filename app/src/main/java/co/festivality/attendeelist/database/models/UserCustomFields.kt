package co.festivality.attendeelist.database.models

import android.os.Parcelable
import io.requery.*

@Entity
interface UserCustomFields : Parcelable, Persistable {

    @get:Key
    @get:Generated
    var id: Int

    @get:OneToOne(mappedBy = "customFields")
    var owner: UserModel

    var fullName: String?
    var firstName: String?
    var lastName: String?
    var email: String?
    var publicEmail: String?
    var company: String?
    var position: String?
    var gender: String?
    var countryCode: String?
    var phone: String?
    var city: String?
    var age: Int?
}