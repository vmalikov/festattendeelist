package co.festivality.attendeelist.database.models

import android.os.Parcelable
import io.requery.*

@Entity
interface UserMediaFiles : Parcelable, Persistable {
    @get:Key
    @get:Generated
    var id: Int

    var default: String?

    @get:ForeignKey
    @get:OneToOne
    var variations: UserMediaFilesVariations?

    @get:OneToOne(mappedBy = "files")
    var userMedia: UserMedia
}