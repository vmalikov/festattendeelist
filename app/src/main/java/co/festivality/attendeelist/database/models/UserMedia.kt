package co.festivality.attendeelist.database.models

import android.os.Parcelable
import io.requery.*

@Entity
interface UserMedia : Parcelable, Persistable {
    @get:Key
    @get:Generated
    var id: Int

    var type: String?
    var label: String?

    @get:ForeignKey
    @get:OneToOne
    var files: UserMediaFiles?

    @get:ManyToOne
    var owner: UserModel

    @get:ForeignKey(references = UserModel::class)
    var userModelId: String
}