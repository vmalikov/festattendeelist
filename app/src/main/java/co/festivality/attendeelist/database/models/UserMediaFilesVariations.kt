package co.festivality.attendeelist.database.models

import io.requery.Entity
import io.requery.Generated
import io.requery.Key
import io.requery.OneToOne

@Entity
interface UserMediaFilesVariations {
    @get:Key
    @get:Generated
    var id: Int

    var small: String?
    var original: String?
    var tiny: String?

    @get:OneToOne(mappedBy = "variations")
    var userMediaFiles: UserMediaFiles
}