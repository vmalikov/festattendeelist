package co.festivality.attendeelist.database.models

import android.os.Parcelable
import io.requery.*

@Entity
interface UserType : Parcelable, Persistable {

    @get:Key
    @get:Generated
    var id: Int

    var text: String

    @get:ManyToOne
    var owner: UserModel

    @get:ForeignKey(references = UserModel::class)
    var userModelId: String
}