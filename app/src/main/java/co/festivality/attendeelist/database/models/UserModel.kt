package co.festivality.attendeelist.database.models

import android.os.Parcelable
import io.requery.*

@Entity
interface UserModel : Parcelable, Persistable {

    @get:Key
    var id: String

    var isFeatured: Int?

    @get:OneToMany
    var type: List<UserType>?

    @get:ForeignKey
    @get:OneToOne
    var customFields: UserCustomFields?

    @get:ForeignKey
    @get:OneToOne
    var userDate: UserDate?

    var likes: Int?

    @get:OneToMany
    var media: List<UserMedia>?

    var ilike: String?
    var irate: String?
}