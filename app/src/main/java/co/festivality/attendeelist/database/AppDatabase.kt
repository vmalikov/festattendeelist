package co.festivality.attendeelist.database

import co.festivality.attendeelist.BaseApplication
import co.festivality.attendeelist.database.models.Models
import co.festivality.attendeelist.database.models.UserModel
import co.festivality.attendeelist.database.models.UserModelEntity
import io.requery.Persistable
import io.requery.android.sqlite.DatabaseSource
import io.requery.reactivex.KotlinReactiveEntityStore
import io.requery.sql.KotlinEntityDataStore
import io.requery.sql.TableCreationMode
import timber.log.Timber

object AppDatabase {
    val data: KotlinReactiveEntityStore<Persistable> by lazy {
        val source = DatabaseSource(BaseApplication.applicationContext(), Models.DEFAULT, 2)
        source.setTableCreationMode(TableCreationMode.CREATE_NOT_EXISTS)
        source.setLoggingEnabled(true)
        KotlinReactiveEntityStore<Persistable>(KotlinEntityDataStore(source.configuration))

    }
}