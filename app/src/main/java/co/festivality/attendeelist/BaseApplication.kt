package co.festivality.attendeelist

import android.app.Application
import android.content.Context
import com.facebook.drawee.backends.pipeline.Fresco
import timber.log.Timber

class BaseApplication : Application() {

    init {
        instance = this
        Timber.plant(Timber.DebugTree())
    }

    override fun onCreate() {
        super.onCreate()
        Fresco.initialize(this)
    }

    companion object {
        private var instance: BaseApplication? = null

        fun applicationContext() : Context {
            return instance!!.applicationContext
        }
    }
}